#include <stdio.h>
#include <ctype.h>
#include <dnasearch.h>

#define MAXLENGTH 15000

int main(int argc, char* argv){
  FILE *text = fopen(argv, "r");
  char dnadata[MAXLENGTH] = {0};
  int positions[MAXLENGTH] = {-1};
  char pattern[MAXLENGTH] = {'\0'};

  int check = rmwhite(text, dnadata);
  if(check != 0){
    return 1;
  }

  while (pattern[0] != 'q' && pattern[0] != 'Q'){
    printf("Enter pattern (or q to quit): ");
    scanf("%s", pattern);
    positions = match(pattern, dnadata);
    printf("\n%s: ", pattern);
  }
}
