#include <dnasearch.h>
#include <ctype.h>
#include <string.h>

#define MAXLENGTH 15000;

char * rmwhite(FILE *text, char *dnadata){
  char dna = '\0';
  if(fscanf(text, "%c", &dna) == EOF){
    printf("EMPTY FILE");
    return 1;
  }

  rewind(text);
  dna = '\0';

  for(int i = 0; i < MAXLENGTH; i++){
    do{
      int ret = fscanf(text, "%c", &dna);
      if(ret = EOF){
        return 0;
      }
    }while (isspace(dna))

    switch(dna){
      case 'A' :
        dnadata[i] = 'A';
        break;

      case 'C' :
        dnadata[i] = 'C';
        break;

      case 'G' :
        dnadata[i] = 'G';
        break;

      case 'T' :
        dnadata[i] = 'T';
        break;

      default :
        printf("ERROR READING FILE");
        return 1;
    }
  }
}

int * match(char *pattern, char *text){
  int plength = strlen(pattern);
  int tlength = strlen(text);
  int Occ[MAXLENGTH] = {0};

  for (int i = 0; i <= tlength - plength; i++){
    bool match = 1;
    int c = 0;
    for (int j = 0; j <= n; j++){
      if (pattern[j] != text[i+j]){
        match = 0;
        break;
      }
    }
    if (match = 1){
      Occ[c] += i;
    }
  }

  return Occ;
}
